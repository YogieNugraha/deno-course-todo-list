import { Application } from "https://deno.land/x/oak/mod.ts";
import route from "./src/routes/router.ts";

const app = new Application();
const port = 3000;

app.use(async (ctx, next) => {
    await next();
    const rt = ctx.response.headers.get("X-Response-Time");
    console.log(`${ctx.request.method} ${ctx.request.url} - ${rt}`);
});

app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.response.headers.set("X-Response-Time", `${ms}ms`);
});

app.use(route.routes());
app.use(route.allowedMethods());

console.log(`Server is running in : http://localhost:${port}`);

await app.listen({port});
