export interface UserSchema {
    _id: { $oid: string };
    name: string;
    email: string;
    phone: string;
    password: string;
}

export function prepareUser(rawUser: UserSchema) {
    return {
        _id: rawUser._id,
        name: rawUser.name,
        email: rawUser.email,
        phone: rawUser.phone,
        password: rawUser.password,
    }
}