export interface TaskSchema {
    _id: { $oid: string };
    titel: string;
    done: boolean;
}

export function prepareTodo(rawTodo: TaskSchema) {
    return {
        _id: rawTodo._id,
        titel: rawTodo.titel,
        done: rawTodo.done
    }
}