import db from "../config/database.ts";
import { UserSchema, prepareUser } from "../models/user.ts";
import * as bcrypt from "https://deno.land/x/bcrypt/mod.ts";
import { validate, required, isString, isEmail, match, minLength } from "https://deno.land/x/validasaur/mod.ts";
import { error500, success200, success204, errorNoBody, error400 } from "../utils/message.ts";

const users = db.collection<UserSchema>('users');

const getUser = async(context: any) => {
    try {
        const data = await users.find(); 
        if (data) {
            context.response.status = 200;
            context.response.body = success200(data);
        } else {
            context.response.status = 204;
        }
    }
    catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const addUser = async (context: any) => {
    if (!context.request.hasBody) {
        context.response.status = 400;
        context.response.body = errorNoBody;
        return
    }
    
    try {
        let body: any = context.request.body();
        const user = await body.value;

        const [ passes, errors ] = await validate(user, {
            name: [required, isString],
            email: [required, isEmail],
            phone: [required, match(/^[a-z0-9]{12}$/)],
            password: [required, isString, minLength(6)]
        });
        if (passes == false) {
            context.response.status = 400;
            context.response.body = error400(errors);
            return
        }
        
        user.password = await bcrypt.hash(user.password)
        
        const id = await users.insertOne(prepareUser(user));

        context.response.status = 201;
        context.response.body = success200({
            id: id, data: prepareUser(user)
        });
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const getUserById: any = async(context: any)=> {
    try {
        let id: string = context.params.id;
        const data = await users.findOne({_id: {"$oid": id}});   
        if (data) {
            context.response.status = 200;
            context.response.body = success200(data);
        } else {
            context.response.status = 204;
        }
    }
    catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const updateUser: any = async(context: any) => {
    if (!context.request.hasBody) {
        context.response.status = 400;
        context.response.body = errorNoBody;
        return
    }
    
    try {
        const id :string = context.params.id;
        let body :any = context.request.body()

        const user = await body.value;
        
        const [ passes, errors ] = await validate(user, {
            name: [isString],
            email: [isEmail],
            phone: [match(/^[a-z0-9]{12}$/)],
            password: [isString, minLength(6)],
            password_before: [required]
        });
        if (passes == false) {
            context.response.status = 400;
            context.response.body = error400(errors);
            return
        }
        
        const result = await users.updateOne({_id: {"$oid": id}}, {$set: prepareUser(user)});    
        
        context.response.status = 200;
        context.response.body = success200(result);
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const deleteUser: any = async(context: any) => {
    try{
        let id :string = context.params.id;
        const result = await users.deleteOne({_id: {"$oid": id}});
        
        context.response.status = 200;
        context.response.body = success200({result});
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const updatePassword: any = async(context: any) => {
    if (!context.request.hasBody) {
        context.response.status = 400;
        context.response.body = errorNoBody;
        return
    }
    
    try {
        const id :string = context.params.id;
        let body :any = context.request.body()

        const user = await body.value;
        
        const [ passes, errors ] = await validate(user, {
            password: [isString, minLength(6)],
            password_before: [required]
        });
        if (passes == false) {
            context.response.status = 400;
            context.response.body = error400(errors);
            return
        }
        
        const get_user = await users.findOne({_id: {"$oid": id}});
        const compare = bcrypt.compareSync(user.password_before, get_user?.password || '');
        if (compare != true) {
            context.response.status = 400;
            context.response.body = error400({ error: "Password Confirmation not match" });
            return
        }
        user.password = await bcrypt.hash(user.password)

        await users.updateOne({_id: {"$oid": id}}, {$set: prepareUser(user)});    
        
        context.response.status = 200;
        context.response.body = success200({ message: 'Password updated' });
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

export {
    getUser,
    addUser,
    getUserById,
    updateUser,
    updatePassword,
    deleteUser
};