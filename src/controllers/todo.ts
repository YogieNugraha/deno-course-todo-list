import db from "../config/database.ts";
import { TaskSchema, prepareTodo } from "../models/todo.ts";
import { validate, required, isBool, isString } from "https://deno.land/x/validasaur/mod.ts";
import { error500, success200, success204, errorNoBody, error400 } from "../utils/message.ts";

const tasks = db.collection<TaskSchema>('tasks');

const getTask = async(context: any) => {
    try {
        const data = await tasks.find(); 
        if (data) {
            context.response.status = 200;
            context.response.body = success200(data);
        } else {
            context.response.status = 204;
        }
    }
    catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const addTask = async (context: any) => {
    if (!context.request.hasBody) {
        context.response.status = 400;
        context.response.body = errorNoBody;
        return
    }
    
    try {
        let body: any = context.request.body();
        const task = await body.value;

        const [ passes, errors ] = await validate(task, {
            titel: [required, isString],
            done: [required, isBool]
        });
        if (passes == false) {
            context.response.status = 400;
            context.response.body = error400(errors);
            return
        }

        const id = await tasks.insertOne(prepareTodo(task));

        context.response.status = 201;
        context.response.body = success200({
            id: id, data: prepareTodo(task)
        });
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const getTaskById: any = async(context: any)=> {
    try {
        let id: string = context.params.id;
        const data = await tasks.findOne({_id: {"$oid": id}});   
        if (data) {
            context.response.status = 200;
            context.response.body = success200(data);
        } else {
            context.response.status = 204;
        }
    }
    catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const updateTask: any = async(context: any) => {
    if (!context.request.hasBody) {
        context.response.status = 400;
        context.response.body = errorNoBody;
        return
    }
    
    try {
        const id :string = context.params.id;
        let body :any = context.request.body()

        const task = await body.value;
        
        const [ passes, errors ] = await validate(task, {
            titel: [isString],
            done: [isBool]
        });
        if (passes == false) {
            context.response.status = 400;
            context.response.body = error400(errors);
            return
        }

        const result = await tasks.updateOne({_id: {"$oid": id}}, {$set: prepareTodo(task)});    
        
        context.response.status = 200;
        context.response.body = success200(result);
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

const deleteTask: any = async(context: any) => {
    try{
        let id :string = context.params.id;
        const result = await tasks.deleteOne({_id: {"$oid": id}});
        
        context.response.status = 200;
        context.response.body = success200({result});
    } catch(e) {
        context.response.status = 500;
        context.response.body = error500(e);
    }
}

export {
    getTask,
    addTask,
    getTaskById,
    updateTask,
    deleteTask
};