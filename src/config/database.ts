import { MongoClient } from "https://deno.land/x/mongo@v0.9.1/mod.ts";
import { config } from "https://deno.land/x/dotenv/mod.ts";

const client = new MongoClient();

client.connectWithUri(config().MONGODB_LINK);

console.log('Database Connected');

const db = client.database('deno-rest');

export default db;