import { config } from "https://deno.land/x/dotenv/mod.ts";

function error500(message: any) {
    const messageE = 'Something went wrong';
    if (config().APP_DEBUG == "true") {
        const messageE = message
    }
    
    return {
        status  : 'fail',
        message : messageE
    };
}

function success200(data: any) {
    return {
        status  : 'success',
        data    : data
    };
}

function success204() {
    return {
        status  : 'success',
        data    : 'Not found'
    };
}

function errorNoBody() {
    return {
        status  : 'fail',
        message : 'Please provide the required data'
    };
}

function error400(message: any) {
    return {
        status  : 'fail',
        message : message
    };
}

export {
    error500,
    success200,
    success204,
    errorNoBody,
    error400
};