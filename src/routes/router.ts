import { Router } from "https://deno.land/x/oak/mod.ts";
import { getUser, addUser, getUserById, updateUser, deleteUser } from "../controllers/user.ts";
import { getTask, addTask, getTaskById, updateTask, deleteTask } from "../controllers/todo.ts";

const route = new Router();

route
    .get('/api/v1/tasks', getTask)
    .post('/api/v1/tasks', addTask)
    .get('/api/v1/tasks/:id', getTaskById)
    .put('/api/v1/tasks/:id', updateTask)
    .delete('/api/v1/tasks/:id', deleteTask);

route
    .get('/api/v1/users', getUser)
    .post('/api/v1/users', addUser)
    .get('/api/v1/users/:id', getUserById)
    .put('/api/v1/users/:id', updateUser)
    .delete('/api/v1/users/:id', deleteUser);

export default route;
